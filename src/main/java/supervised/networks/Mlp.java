/**
 * Multi-Layer Perceptron Network (MLP) 
 * using Backpropagation with stochastic gradient descent.
 * @author Thanasis Lagias
 * @version 1.0
 */

package supervised.networks;

import java.util.ArrayList;
import org.jblas.DoubleMatrix;
import supervised.activation.Activation;
import supervised.activation.Sigmoid;


public class Mlp {
	private int totalLayers;
	private int inputNodes;
	private int hiddenNodes; // all hidden layers have the same number of nodes
	private int outputNodes;
	private double learnRate;
	private Activation function;
	private ArrayList<DoubleMatrix> weights;
	private ArrayList<DoubleMatrix> biases;
	private ArrayList<DoubleMatrix> netVals;
	private ArrayList<DoubleMatrix> actVals;
	private ArrayList<DoubleMatrix> deltas;

	
	/**
	 * Creates a new Multi-Layer Perceptron (Mlp). 
	 * All hidden layers have the same number of neurons.
	 * @param layers Total number of layers
	 * @param inputN Neurons of input layer
	 * @param hiddenN Neurons of hidden layer(s)
	 * @param outputN Neurons of output layer
	 */
	public Mlp(int layers, int inputN, int hiddenN, int outputN) {
		totalLayers = layers;
		inputNodes  = inputN;
		hiddenNodes = hiddenN;
		outputNodes = outputN;
		learnRate = 0.1d; // default
		function = new Sigmoid(); // default
		weights  = new ArrayList<DoubleMatrix>();
		biases   = new ArrayList<DoubleMatrix>();
		netVals  = new ArrayList<DoubleMatrix>();
		actVals  = new ArrayList<DoubleMatrix>();
		deltas   = new ArrayList<DoubleMatrix>();

		// create all layers
		for (int layer = 0; layer < totalLayers; layer++) {
			// input layer
			if (layer == 0) {
				// no weights, no biases
				weights.add(DoubleMatrix.ones(inputNodes));
				biases.add(DoubleMatrix.ones(inputNodes));
				netVals.add(DoubleMatrix.ones(inputNodes));
				actVals.add(DoubleMatrix.ones(inputNodes));
				deltas.add(DoubleMatrix.ones(inputNodes));
			}
			// hidden layers
			if (layer == 1) {
				weights.add(DoubleMatrix.rand(hiddenNodes, inputNodes));
				biases.add(DoubleMatrix.zeros(hiddenNodes));
				netVals.add(DoubleMatrix.randn(hiddenNodes));
				actVals.add(DoubleMatrix.randn(hiddenNodes));
				deltas.add(DoubleMatrix.ones(hiddenNodes));
			}
			if (1 < layer && layer < totalLayers - 1) {
				weights.add(DoubleMatrix.randn(hiddenNodes, hiddenNodes));
				biases.add(DoubleMatrix.zeros(hiddenNodes));
				netVals.add(DoubleMatrix.randn(hiddenNodes));
				actVals.add(DoubleMatrix.randn(hiddenNodes));
				deltas.add(DoubleMatrix.ones(hiddenNodes));
			}
			// output layer
			if (layer == totalLayers - 1) {
				weights.add(DoubleMatrix.randn(outputNodes, hiddenNodes));
				biases.add(DoubleMatrix.zeros(outputNodes));
				netVals.add(DoubleMatrix.randn(outputNodes));
				actVals.add(DoubleMatrix.randn(outputNodes));
				deltas.add(DoubleMatrix.ones(outputNodes));
			}
		}
	}

	
	/**
	 * Feed-forwards the inputs through the MLP network. 
	 * @param inputArray An array of network inputs
	 * @return An array of network outputs
	 */
	public double[] forwardProp(double[] inputArray) {
		DoubleMatrix layerVals = null;
		DoubleMatrix inputs = new DoubleMatrix(inputArray);

		// scan forward all layers
		for (int layer = 0; layer < totalLayers; layer++) {
			// input layer
			if (layer == 0) {
				netVals.set(layer, inputs);
				actVals.set(layer, inputs);
			}
			// hidden & output layers
			if (0 < layer && layer < totalLayers) {
				layerVals = weights.get(layer).mmul(actVals.get(layer - 1));
				layerVals = layerVals.add(biases.get(layer));
				netVals.set(layer, layerVals);
				layerVals = function.activation(layerVals);
				actVals.set(layer, layerVals);
			}
		}
		return layerVals.toArray();
	}

	
	/**
	 * Receives a single pair of input-target values 
	 * and trains the network using back-propagation.
	 * @param inputArray An array of input values
	 * @param targetArray An array of target values (labels)
	 */
	public void backwardProp(double[] inputArray, double[] targetArray) {
		DoubleMatrix outputs = new DoubleMatrix(forwardProp(inputArray));
		DoubleMatrix targets = new DoubleMatrix(targetArray);
		// assume cost function is the summed squared error (SSE)
		DoubleMatrix delta = null;
		DoubleMatrix deltaWeights = null;
		DoubleMatrix deltaBiases = null;

		// scan backwards all layers
		for (int layer = totalLayers - 1; layer > 0; layer--) {
			// output layer
			if (layer == totalLayers - 1) {
				delta = targets.sub(outputs);
				delta = delta.mul(function.derivative(netVals.get(layer)));
				deltas.set(layer, delta);
				deltaWeights = deltas.get(layer).mmul(actVals.get(layer - 1).transpose());
				deltaWeights = deltaWeights.mul(learnRate);
				weights.set(layer, weights.get(layer).add(deltaWeights));
				deltaBiases = deltas.get(layer).mul(learnRate);
				biases.set(layer, biases.get(layer).add(deltaBiases));
			}
			// hidden layers
			if (0 < layer && layer < totalLayers - 1) {
				delta = weights.get(layer + 1).transpose().mmul(deltas.get(layer + 1));
				delta = delta.mul(function.derivative(netVals.get(layer)));
				deltas.set(layer, delta);
				deltaWeights = deltas.get(layer).mmul(actVals.get(layer - 1).transpose());
				deltaWeights = deltaWeights.mul(learnRate);
				weights.set(layer, weights.get(layer).add(deltaWeights));
				deltaBiases = deltas.get(layer).mul(learnRate);
				biases.set(layer, biases.get(layer).add(deltaBiases));
			}
			// input layer (layer 0)
			// no weights, no biases, no update

		}
	}

	
	//////////////////////////////////////////////////////////////////////
	// Setters
	//////////////////////////////////////////////////////////////////////
	/**
	 * Sets the Activation function of all layers.
	 * @param f An object that implements the Activation interface
	 */
	public void setActivationFunction(Activation f) {
		function = f;
	}

	/**
	 * Sets the Learning Rate of the network.
	 * @param value The learning rate
	 */
	public void setLearningRate(double value) {
		learnRate = value;
	}
}
