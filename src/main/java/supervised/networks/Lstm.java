/**
 * Long-Short Term Memory Network (LSTM)
 * using Backpropagation through time (BPTT).
 * @author Thanasis Lagias
 * @version 1.0
 */

package supervised.networks;

import java.util.ArrayList;
import org.jblas.DoubleMatrix;
import supervised.activation.Activation;
import supervised.activation.Sigmoid;
import supervised.activation.Tanh;


public class Lstm {
	private int inputNodes;
	private int outputNodes;
	private double learnRate;

	// inputs & outputs
	private DoubleMatrix xActVals;  // x
	private DoubleMatrix hActVals;  // h
	// forget gate
	private DoubleMatrix fxWeights; // W_f
	private DoubleMatrix fhWeights; // U_f
	private DoubleMatrix fBiases;   // b_f
	private DoubleMatrix fNetVals;  // net_f
	private DoubleMatrix fActVals;  // sigma_f
	private Activation fFunction;   // sigmoid
	// input gate
	private DoubleMatrix ixWeights;
	private DoubleMatrix ihWeights;
	private DoubleMatrix iBiases;
	private DoubleMatrix iNetVals;
	private DoubleMatrix iActVals;
	private Activation iFunction; // sigmoid
	// block input
	private DoubleMatrix zxWeights;
	private DoubleMatrix zhWeights;
	private DoubleMatrix zBiases;
	private DoubleMatrix zNetVals;
	private DoubleMatrix zActVals;
	private Activation zFunction; // tanh
	// output gate
	private DoubleMatrix oxWeights;
	private DoubleMatrix ohWeights;
	private DoubleMatrix oBiases;
	private DoubleMatrix oNetVals;
	private DoubleMatrix oActVals;
	private Activation oFunction; // sigmoid
	// cell
	private DoubleMatrix cActVals; // C
	private Activation cFunction; // tanh
	// lists
	private ArrayList<DoubleMatrix> fNetList;
	private ArrayList<DoubleMatrix> fActList;
	private ArrayList<DoubleMatrix> iNetList;
	private ArrayList<DoubleMatrix> iActList;
	private ArrayList<DoubleMatrix> zNetList;
	private ArrayList<DoubleMatrix> zActList;
	private ArrayList<DoubleMatrix> oNetList;
	private ArrayList<DoubleMatrix> oActList;
	private ArrayList<DoubleMatrix> cActList;

	
	/**
	 * Creates a new Long-Short Term Memory Network (LSTM).
	 * @param inputN Neurons of input layer
	 * @param outputN Neurons of output layer
	 */
	public Lstm(int inputN, int outputN) {
		inputNodes  = inputN;
		outputNodes = outputN;
		learnRate   = 0.1f; // default
		// inputs & outputs
		xActVals  = DoubleMatrix.zeros(inputNodes);
		hActVals  = DoubleMatrix.zeros(outputNodes);
		// forget gate
		fxWeights = DoubleMatrix.randn(outputNodes, inputNodes);
		fhWeights = DoubleMatrix.randn(outputNodes, outputNodes);
		fBiases   = DoubleMatrix.zeros(outputNodes);
		fNetVals  = DoubleMatrix.zeros(outputNodes);
		fActVals  = DoubleMatrix.zeros(outputNodes);
		fFunction = new Sigmoid();
		// input gate
		ixWeights = DoubleMatrix.randn(outputNodes, inputNodes);
		ihWeights = DoubleMatrix.randn(outputNodes, outputNodes);
		iBiases   = DoubleMatrix.zeros(outputNodes);
		iNetVals  = DoubleMatrix.zeros(outputNodes);
		iActVals  = DoubleMatrix.zeros(outputNodes);
		iFunction = new Sigmoid();
		// block input
		zxWeights = DoubleMatrix.randn(outputNodes, inputNodes);
		zhWeights = DoubleMatrix.randn(outputNodes, outputNodes);
		zBiases   = DoubleMatrix.zeros(outputNodes);
		zNetVals  = DoubleMatrix.zeros(outputNodes);
		zActVals  = DoubleMatrix.zeros(outputNodes);
		zFunction = new Tanh();
		// output gate
		oxWeights = DoubleMatrix.randn(outputNodes, inputNodes);
		ohWeights = DoubleMatrix.randn(outputNodes, outputNodes);
		oBiases   = DoubleMatrix.zeros(outputNodes);
		oNetVals  = DoubleMatrix.zeros(outputNodes);
		oActVals  = DoubleMatrix.zeros(outputNodes);
		oFunction = new Sigmoid();
		// final output
		cActVals  = DoubleMatrix.zeros(outputNodes);
		cFunction = new Tanh();
		// lists
		fNetList = new ArrayList<DoubleMatrix>();
		fActList = new ArrayList<DoubleMatrix>();
		iNetList = new ArrayList<DoubleMatrix>();
		iActList = new ArrayList<DoubleMatrix>();
		zNetList = new ArrayList<DoubleMatrix>();
		zActList = new ArrayList<DoubleMatrix>();
		oNetList = new ArrayList<DoubleMatrix>();
		oActList = new ArrayList<DoubleMatrix>();
		cActList = new ArrayList<DoubleMatrix>();
	}

	
	/**
	 * Propagates forward the inputs through the LSTM and generates a prediction.
	 * @param inputList An ArrayList containing sets of inputs in sequential order
	 * @return An ArrayList containing predicted sets of outputs in sequential order
	 */
	public ArrayList<double[]> forwardProp(ArrayList<double[]> inputList) {
		ArrayList<double[]> results = new ArrayList<double[]>();
		int timesteps = inputList.size();
		prepareLists(timesteps);

		for (int t = 1; t <= timesteps; t++) {
			xActVals = new DoubleMatrix(inputList.get(t - 1));
			// forget gate
			fNetVals = fBiases
					.add(fxWeights.mmul(xActVals))
					.add(fhWeights.mmul(hActVals));
			fActVals = fFunction.activation(fNetVals);
			
			// input gate
			iNetVals = iBiases
					.add(ixWeights.mmul(xActVals))
					.add(ihWeights.mmul(hActVals));
			iActVals = iFunction.activation(iNetVals);
			
			// block input
			zNetVals = zBiases
					.add(zxWeights.mmul(xActVals))
					.add(zhWeights.mmul(hActVals));
			zActVals = zFunction.activation(zNetVals);
			
			// output gate
			oNetVals = oBiases
					.add(oxWeights.mmul(xActVals))
					.add(ohWeights.mmul(hActVals));
			oActVals = oFunction.activation(oNetVals);
			
			// cell state update
			cActVals = fActVals
					.mul(cActVals)
					.add(iActVals.mul(zActVals));
			
			// output update
			hActVals = oActVals.mul(cFunction.activation(cActVals));
			
			// get results
			results.add(hActVals.toArray());
			
			// update lists
			fNetList.set(t, fNetVals);
			fActList.set(t, fActVals);
			iNetList.set(t, iNetVals);
			iActList.set(t, iActVals);
			zNetList.set(t, zNetVals);
			zActList.set(t, zActVals);
			oNetList.set(t, oNetVals);
			oActList.set(t, oActVals);
			cActList.set(t, cActVals);
		}
		return results;
	}

	
	/**
	 * Receives two lists with sequences of inputs-target sets 
	 * and trains the network using back-propagation through time (BPTT).
	 * @param inputList An ArrayList containing sets of inputs in sequential order
	 * @param targetList An ArrayList containing sets of target values in sequential order
	 */
	public void backwardProp(ArrayList<double[]> inputList, ArrayList<double[]> targetList) {
		ArrayList<double[]> outputList = forwardProp(inputList);
		// t deltas
		DoubleMatrix delta = DoubleMatrix.zeros(outputNodes);
		DoubleMatrix delta_h = DoubleMatrix.zeros(outputNodes);
		DoubleMatrix delta_f = DoubleMatrix.zeros(outputNodes);
		DoubleMatrix delta_i = DoubleMatrix.zeros(outputNodes);
		DoubleMatrix delta_z = DoubleMatrix.zeros(outputNodes);
		DoubleMatrix delta_o = DoubleMatrix.zeros(outputNodes);
		DoubleMatrix delta_c = DoubleMatrix.zeros(outputNodes);
		// t+1 deltas
		DoubleMatrix deltA_f = DoubleMatrix.zeros(outputNodes);
		DoubleMatrix deltA_i = DoubleMatrix.zeros(outputNodes);
		DoubleMatrix deltA_z = DoubleMatrix.zeros(outputNodes);
		DoubleMatrix deltA_o = DoubleMatrix.zeros(outputNodes);
		// weight deltas
		DoubleMatrix delta_fxWeights = DoubleMatrix.zeros(outputNodes, inputNodes);
		DoubleMatrix delta_ixWeights = DoubleMatrix.zeros(outputNodes, inputNodes);
		DoubleMatrix delta_zxWeights = DoubleMatrix.zeros(outputNodes, inputNodes);
		DoubleMatrix delta_oxWeights = DoubleMatrix.zeros(outputNodes, inputNodes);
		DoubleMatrix delta_fhWeights = DoubleMatrix.zeros(outputNodes, outputNodes);
		DoubleMatrix delta_ihWeights = DoubleMatrix.zeros(outputNodes, outputNodes);
		DoubleMatrix delta_zhWeights = DoubleMatrix.zeros(outputNodes, outputNodes);
		DoubleMatrix delta_ohWeights = DoubleMatrix.zeros(outputNodes, outputNodes);
		// bias deltas
		DoubleMatrix delta_fBiases = DoubleMatrix.zeros(outputNodes);
		DoubleMatrix delta_iBiases = DoubleMatrix.zeros(outputNodes);
		DoubleMatrix delta_zBiases = DoubleMatrix.zeros(outputNodes);
		DoubleMatrix delta_oBiases = DoubleMatrix.zeros(outputNodes);
		int timesteps = targetList.size();

		for (int t = timesteps; t >= 1; t--) {
			DoubleMatrix inputs  = new DoubleMatrix(inputList.get(t - 1));
			DoubleMatrix targets = new DoubleMatrix(targetList.get(t - 1));
			DoubleMatrix outputs = new DoubleMatrix(outputList.get(t - 1));
			delta = targets.sub(outputs);
			deltA_f = delta_f;
			deltA_i = delta_i;
			deltA_z = delta_z;
			deltA_o = delta_o;

			delta_h = delta
					.add(fhWeights.transpose().mmul(delta_f))
					.add(ihWeights.transpose().mmul(delta_i))
					.add(zhWeights.transpose().mmul(delta_z))
					.add(ohWeights.transpose().mmul(delta_o));

			delta_o = delta_h
					.mul(cFunction.activation(cActList.get(t)))
					.mul(oFunction.derivative(oNetList.get(t)));

			delta_c = delta_h
					.mul(oActList.get(t))
					.mul(cFunction.derivative(cActList.get(t)))
					.add(delta_c.mul(fActList.get(t + 1)));

			delta_f = delta_c
					.mul(cActList.get(t - 1))
					.mul(fFunction.derivative(fNetList.get(t)));

			delta_i = delta_c
					.mul(zActList.get(t))
					.mul(iFunction.derivative(iNetList.get(t)));

			delta_z = delta_c
					.mul(iActList.get(t))
					.mul(zFunction.derivative(zNetList.get(t)));

			// sum weight gradients
			delta_fxWeights = delta_fxWeights.add(delta_f.mmul(inputs.transpose()));
			delta_ixWeights = delta_ixWeights.add(delta_i.mmul(inputs.transpose()));
			delta_zxWeights = delta_zxWeights.add(delta_z.mmul(inputs.transpose()));
			delta_oxWeights = delta_oxWeights.add(delta_o.mmul(inputs.transpose()));
			if (t < timesteps) {
				delta_fhWeights = delta_fhWeights.add(deltA_f.mmul(outputs.transpose()));
				delta_ihWeights = delta_ihWeights.add(deltA_i.mmul(outputs.transpose()));
				delta_zhWeights = delta_zhWeights.add(deltA_z.mmul(outputs.transpose()));
				delta_ohWeights = delta_ohWeights.add(deltA_o.mmul(outputs.transpose()));
			}
			delta_fBiases = delta_fBiases.add(delta_f);
			delta_iBiases = delta_iBiases.add(delta_i);
			delta_zBiases = delta_zBiases.add(delta_z);
			delta_oBiases = delta_oBiases.add(delta_o);
		}

		// update weights & biases
		fxWeights = fxWeights.add(delta_fxWeights.mul(learnRate));
		ixWeights = ixWeights.add(delta_ixWeights.mul(learnRate));
		zxWeights = zxWeights.add(delta_zxWeights.mul(learnRate));
		oxWeights = oxWeights.add(delta_oxWeights.mul(learnRate));
		fhWeights = fhWeights.add(delta_fhWeights.mul(learnRate));
		ihWeights = ihWeights.add(delta_ihWeights.mul(learnRate));
		zhWeights = zhWeights.add(delta_zhWeights.mul(learnRate));
		ohWeights = ohWeights.add(delta_ohWeights.mul(learnRate));
		fBiases = fBiases.add(delta_fBiases.mul(learnRate));
		iBiases = iBiases.add(delta_iBiases.mul(learnRate));
		zBiases = zBiases.add(delta_zBiases.mul(learnRate));
		oBiases = oBiases.add(delta_oBiases.mul(learnRate));
	}

	
	//////////////////////////////////////////////////////////////////////
	// Setters
	//////////////////////////////////////////////////////////////////////
	/**
	 * Sets the Learning Rate of the network.
	 * @param value The learning rate
	 */
	public void setLearningRate(float value) {
		learnRate = value;
	}

	
	//////////////////////////////////////////////////////////////////////
	// Private Part
	//////////////////////////////////////////////////////////////////////
	private void prepareLists(int steps) {
		fNetList.clear();
		fActList.clear();
		iNetList.clear();
		iActList.clear();
		zNetList.clear();
		zActList.clear();
		oNetList.clear();
		oActList.clear();
		cActList.clear();
		for (int i = 0; i <= steps + 1; i++) {
			fNetList.add(DoubleMatrix.zeros(outputNodes));
			fActList.add(DoubleMatrix.zeros(outputNodes));
			iNetList.add(DoubleMatrix.zeros(outputNodes));
			iActList.add(DoubleMatrix.zeros(outputNodes));
			zNetList.add(DoubleMatrix.zeros(outputNodes));
			zActList.add(DoubleMatrix.zeros(outputNodes));
			oNetList.add(DoubleMatrix.zeros(outputNodes));
			oActList.add(DoubleMatrix.zeros(outputNodes));
			cActList.add(cActVals);
		}
	}
}
