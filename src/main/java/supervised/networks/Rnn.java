/**
 * Vanilla Recurrent Neural Network (RNN)
 * using Backpropagation through time (BPTT).
 * @author Thanasis Lagias
 * @version 1.0
 */

package supervised.networks;

import java.util.ArrayList;
import org.jblas.DoubleMatrix;
import supervised.activation.Activation;
import supervised.activation.Sigmoid;
import supervised.activation.Tanh;


public class Rnn {
	private int inputNodes;
	private int hiddenNodes;
	private int outputNodes;
	private double learnRate;
	private Activation hiddenFunction;
	private Activation outputFunction;
	// inputs
	private DoubleMatrix xActVals;
	private DoubleMatrix xWeights;
	// hidden state (recurrent)
	private DoubleMatrix hWeights;
	private DoubleMatrix hBiases;
	private DoubleMatrix hNetVals;
	private DoubleMatrix hActVals;
	// outputs
	private DoubleMatrix yWeights;
	private DoubleMatrix yBiases;
	private DoubleMatrix yNetVals;
	private DoubleMatrix yActVals;
	// lists
	private ArrayList<DoubleMatrix> xActList;
	private ArrayList<DoubleMatrix> hNetList;
	private ArrayList<DoubleMatrix> hActList;
	private ArrayList<DoubleMatrix> yNetList;

	
	/**
	 * Creates a new Recurrent Neural Network (RNN).
	 * @param inputN Neurons of input layer
	 * @param hiddenN Neurons of hidden (recurrent) layer
	 * @param outputN Neurons of output layer
	 */
	public Rnn(int inputN, int hiddenN, int outputN) {
		inputNodes  = inputN;
		hiddenNodes = hiddenN;
		outputNodes = outputN;
		learnRate = 0.1d; // default
		hiddenFunction = new Tanh();
		outputFunction = new Sigmoid();
		// inputs
		xActVals = DoubleMatrix.zeros(inputNodes);
		xWeights = DoubleMatrix.randn(hiddenNodes, inputNodes); // non-recurrent weights
		// hidden state
		hWeights = DoubleMatrix.randn(hiddenNodes, hiddenNodes); // recurrent weights
		hBiases  = DoubleMatrix.zeros(hiddenNodes);
		hNetVals = DoubleMatrix.zeros(hiddenNodes);
		hActVals = DoubleMatrix.zeros(hiddenNodes); // recurrent state
		// outputs
		yWeights = DoubleMatrix.randn(outputNodes, hiddenNodes);
		yBiases  = DoubleMatrix.zeros(outputNodes);
		yNetVals = DoubleMatrix.zeros(outputNodes);
		yActVals = DoubleMatrix.zeros(outputNodes);
		// lists
		xActList = new ArrayList<DoubleMatrix>();
		hNetList = new ArrayList<DoubleMatrix>();
		hActList = new ArrayList<DoubleMatrix>();
		yNetList = new ArrayList<DoubleMatrix>();
	}

	
	/**
	 * Propagates forward the inputs through the RNN and generates a prediction. 
	 * @param inputList An ArrayList containing sets of inputs in sequential order
	 * @return An ArrayList containing predicted sets of outputs in sequential order
	 */
	public ArrayList<double[]> forwardProp(ArrayList<double[]> inputList) {
		ArrayList<double[]> results = new ArrayList<double[]>();
		int timesteps = inputList.size();
		DoubleMatrix xNetPart = null;
		DoubleMatrix hNetPart = null;
		prepareLists(timesteps);

		for (int t = 1; t <= timesteps; t++) {
			xActVals = new DoubleMatrix(inputList.get(t - 1));
			xNetPart = xWeights.mmul(xActVals);
			hNetPart = hWeights.mmul(hActVals);
			hNetVals = xNetPart.add(hNetPart).add(hBiases);
			hActVals = hiddenFunction.activation(hNetVals);
			yNetVals = yWeights.mmul(hActVals).add(yBiases);
			yActVals = outputFunction.activation(yNetVals);
			xActList.set(t, xActVals);
			hNetList.set(t, hNetVals);
			hActList.set(t, hActVals);
			yNetList.set(t, yNetVals);
			results.add(yActVals.toArray());
		}
		return results;
	}

	
	/**
	 * Receives two lists with sequences of inputs-targets sets 
	 * and trains the network using back-propagation through time (BPTT).
	 * @param inputList An ArrayList containing sets of inputs in sequential order
	 * @param targetList An ArrayList containing sets of target values in sequential order
	 */
	public void backwardProp(ArrayList<double[]> inputList, ArrayList<double[]> targetList) {
		ArrayList<double[]> outputList = forwardProp(inputList);
		DoubleMatrix delta_xWeights = DoubleMatrix.zeros(hiddenNodes, inputNodes);
		DoubleMatrix delta_hWeights = DoubleMatrix.zeros(hiddenNodes, hiddenNodes);
		DoubleMatrix delta_hBiases  = DoubleMatrix.zeros(hiddenNodes);
		DoubleMatrix delta_yWeights = DoubleMatrix.zeros(outputNodes, hiddenNodes);
		DoubleMatrix delta_yBiases  = DoubleMatrix.zeros(outputNodes);
		DoubleMatrix delta0 = null;
		DoubleMatrix delta = null;
		int timesteps = targetList.size();

		for (int t = timesteps; t >= 1; t--) {
			DoubleMatrix targets = new DoubleMatrix(targetList.get(t - 1));
			DoubleMatrix outputs = new DoubleMatrix(outputList.get(t - 1));
			DoubleMatrix grad_xWeights = DoubleMatrix.zeros(hiddenNodes, inputNodes);
			DoubleMatrix grad_hWeights = DoubleMatrix.zeros(hiddenNodes, hiddenNodes);
			DoubleMatrix grad_hBiases  = DoubleMatrix.zeros(hiddenNodes);
			DoubleMatrix grad_yWeights = DoubleMatrix.zeros(outputNodes, hiddenNodes);
			DoubleMatrix grad_yBiases  = DoubleMatrix.zeros(outputNodes);
			// compute delta0
			delta0 = targets.sub(outputs);
			delta0 = delta0.mul(outputFunction.derivative(yNetList.get(t)));

			for (int i = t; i >= 1; i--) {
				// compute deltas
				if (i == t) {
					delta = yWeights.transpose().mmul(delta0);
					delta = delta.mul(hiddenFunction.derivative(hNetList.get(i)));
				} else {
					delta = hWeights.transpose().mmul(delta);
					delta = delta.mul(hiddenFunction.derivative(hNetList.get(i)));
				}
				// sum parts of weight gradients
				grad_xWeights = grad_xWeights.add(delta.mmul(xActList.get(i).transpose()));
				grad_hWeights = grad_hWeights.add(delta.mmul(hActList.get(i - 1).transpose()));
				grad_hBiases  = grad_hBiases.add(delta);
			}

			grad_yWeights = delta0.mmul(hActList.get(t).transpose());
			grad_yBiases  = delta0;

			// sum weight gradients
			delta_xWeights = delta_xWeights.add(grad_xWeights);
			delta_hWeights = delta_hWeights.add(grad_hWeights);
			delta_hBiases  = delta_hBiases.add(grad_hBiases);
			delta_yBiases  = delta_yBiases.add(grad_yBiases);
			delta_yWeights = delta_yWeights.add(grad_yWeights);
		}

		// update weights
		xWeights = xWeights.add(delta_xWeights.mul(learnRate));
		hWeights = hWeights.add(delta_hWeights.mul(learnRate));
		hBiases  = hBiases.add(delta_hBiases.mul(learnRate));
		yBiases  = yBiases.add(delta_yBiases.mul(learnRate));
		yWeights = yWeights.add(delta_yWeights.mul(learnRate));
	}

	
	//////////////////////////////////////////////////////////////////////
	// Setters
	//////////////////////////////////////////////////////////////////////
	/**
	 * Sets the Activation function of the hidden (recurrent) layer.
	 * @param f An object that implements the Activation interface
	 */
	public void setHiddenActivationFunction(Activation f) {
		hiddenFunction = f;
	}

	/**
	 * Sets the Activation function of the output layer.
	 * @param f An object that implements the Activation interface
	 */
	public void setOutputActivationFunction(Activation f) {
		outputFunction = f;
	}

	/**
	 * Sets the Learning Rate of the network.
	 * @param value The learning rate
	 */
	public void setLearningRate(double value) {
		learnRate = value;
	}

	
	//////////////////////////////////////////////////////////////////////
	// Private Part
	//////////////////////////////////////////////////////////////////////
	private void prepareLists(int steps) {
		xActList.clear();
		hNetList.clear();
		hActList.clear();
		yNetList.clear();
		for (int i = 0; i <= steps; i++) {
			xActList.add(new DoubleMatrix());
			hNetList.add(new DoubleMatrix());
			hActList.add(new DoubleMatrix());
			yNetList.add(new DoubleMatrix());
		}
		hActList.set(0, hActVals);
	}
}
