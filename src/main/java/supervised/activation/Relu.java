/**
 * Rectified Linear Unit(ReLU) activation function
 * @see https://en.wikipedia.org/wiki/Activation_function
 * @author Thanasis Lagias
 * @version 1.0
 */

package supervised.activation;

import org.jblas.DoubleMatrix;


public class Relu implements Activation{
	
	/**
	 * Creates a ReLU activation function.
	*/
	public Relu() {
		//
	}

	
	/**
	 * {@inheritDoc}
	 */
	public double activation(double x) {
		if (x >= 0) return x;
		if (x < 0)  return 0;
		return 1;
	}

	
	/**
	 * {@inheritDoc}
	 */
	public double derivative(double x) {
		if (x >= 0) return 1;
		if (x < 0)  return 0;
		return 1;
	}

	
	/**
	 * {@inheritDoc}
	 */
	public DoubleMatrix activation(DoubleMatrix m) {
		DoubleMatrix result = DoubleMatrix.zeros(m.getLength());
		for (int i = 0; i < m.getLength(); i++) {
			result.put(i, this.activation(m.get(i)));
		}
		return result;
	}

	
	/**
	 * {@inheritDoc}
	 */
	public DoubleMatrix derivative(DoubleMatrix m) {
		DoubleMatrix result = DoubleMatrix.zeros(m.getLength());
		for (int i = 0; i < m.getLength(); i++) {
			result.put(i, this.derivative(m.get(i)));
		}
		return result;
	}
}
