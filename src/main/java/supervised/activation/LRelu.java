/**
 * Leaky Rectified Linear Unit(Leaky ReLU) activation function
 * @see https://en.wikipedia.org/wiki/Activation_function
 * @author Thanasis Lagias
 * @version 1.0
 */

package supervised.activation;

import org.jblas.DoubleMatrix;


public class LRelu implements Activation{
	
	/**
	 * Creates a LRelu activation function.
	*/
	public LRelu() {
		//
	}

	
	/**
	 * {@inheritDoc}
	 */
	public double activation(double x) {
		if (x >= 0) return x;
		if (x < 0)  return 0.01f * x;
		return 1;
	}

	
	/**
	 * {@inheritDoc}
	 */
	public double derivative(double x) {
		if (x >= 0) return 1;
		if (x < 0)  return 0.01f;
		return 1;
	}

	
	/**
	 * {@inheritDoc}
	 */
	public DoubleMatrix activation(DoubleMatrix m) {
		DoubleMatrix result = DoubleMatrix.zeros(m.getLength());
		for (int i = 0; i < m.getLength(); i++) {
			result.put(i, this.activation(m.get(i)));
		}
		return result;
	}

	
	/**
	 * {@inheritDoc}
	 */
	public DoubleMatrix derivative(DoubleMatrix m) {
		DoubleMatrix result = DoubleMatrix.zeros(m.getLength());
		for (int i = 0; i < m.getLength(); i++) {
			result.put(i, this.derivative(m.get(i)));
		}
		return result;
	}
}
