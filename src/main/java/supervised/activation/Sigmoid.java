/**
 * Sigmoid activation function
 * @see https://en.wikipedia.org/wiki/Activation_function
 * @author Thanasis Lagias
 * @version 1.0
 */

package supervised.activation;

import org.jblas.DoubleMatrix;

import supervised.utilities.Core;


public class Sigmoid implements Activation{
	
	/**
	 * Creates a Sigmoid activation function.
	*/
	public Sigmoid() {
		//
	}

	
	/**
	 * {@inheritDoc}
	 */
	public double activation(double x) {
		double s = 1 / (1 + Core.exp(-x));
		return s;
	}

	
	/**
	 * {@inheritDoc}
	 */
	public double derivative(double x) {
		double s = 1 / (1 + Core.exp(-x));
		return s * (1 - s);
	}

	
	/**
	 * {@inheritDoc}
	 */
	public DoubleMatrix activation(DoubleMatrix m) {
		DoubleMatrix result = DoubleMatrix.zeros(m.getLength());
		for (int i = 0; i < m.getLength(); i++) {
			result.put(i, this.activation(m.get(i)));
		}
		return result;
	}

	
	/**
	 * {@inheritDoc}
	 */
	public DoubleMatrix derivative(DoubleMatrix m) {
		DoubleMatrix result = DoubleMatrix.zeros(m.getLength());
		for (int i = 0; i < m.getLength(); i++) {
			result.put(i, this.derivative(m.get(i)));
		}
		return result;
	}
}
