/**
 * TanH activation function
 * @see https://en.wikipedia.org/wiki/Activation_function
 * @author Thanasis Lagias
 * @version 1.0
 */

package supervised.activation;

import org.jblas.DoubleMatrix;

import supervised.utilities.Core;


public class Tanh implements Activation{
	
	/**
	 * Creates a TanH activation function.
	*/
	public Tanh() {
		//
	}

	
	/**
	 * {@inheritDoc}
	 */
	public double activation(double x) {
		double t = (Core.exp(x) - Core.exp(-x)) / (Core.exp(x) + Core.exp(-x));
		return t;
	}

	
	/**
	 * {@inheritDoc}
	 */
	public double derivative(double x) {
		double t = (Core.exp(x) - Core.exp(-x)) / (Core.exp(x) + Core.exp(-x));
		return 1 - Core.pow(t, 2);
	}

	
	/**
	 * {@inheritDoc}
	 */
	public DoubleMatrix activation(DoubleMatrix m) {
		DoubleMatrix result = DoubleMatrix.zeros(m.getLength());
		for (int i = 0; i < m.getLength(); i++) {
			result.put(i, this.activation(m.get(i)));
		}
		return result;
	}

	
	/**
	 * {@inheritDoc}
	 */
	public DoubleMatrix derivative(DoubleMatrix m) {
		DoubleMatrix result = DoubleMatrix.zeros(m.getLength());
		for (int i = 0; i < m.getLength(); i++) {
			result.put(i, this.derivative(m.get(i)));
		}
		return result;
	}
}
