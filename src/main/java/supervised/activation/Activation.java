package supervised.activation;

import org.jblas.DoubleMatrix;


public interface Activation {
	
	/**
	 * Applies the activation function on a double number.
	 * @param x  A double number
	 * @return Computed value of the activation function
	*/
	public double activation(double x);
	
	
	/**
	 * Computes the activation function derivative for a double number.
	 * @param x  A double number
	 * @return Computed value of the derivative
	*/
	public double derivative(double x);
	
	
	/**
	 * Applies the activation function on a vector of doubles.
	 * @param m A vector of doubles
	 * @return A vector of the resulted values  
	*/
	public DoubleMatrix activation(DoubleMatrix m);
	
	
	/**
	 * Computes the activation function derivatives for a vector of doubles.
	 * @param m A vector of doubles
	 * @return A vector of the resulted values  
	*/
	public DoubleMatrix derivative(DoubleMatrix m);
}
