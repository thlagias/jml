package supervised.utilities;

public class Core {
	public static double exp(double x) {
		return Math.exp(x);
	}
	
	public static double pow(double base, double exponent) {
		return Math.pow(base, exponent);
	}
	
	public static double random(double min, double max) {
		double range = (max - min) + 1;     
		return (Math.random() * range) + min;
	}
	
	public static double random(double max) {
		double range = max + 1;     
		return (Math.random() * range);
	}
	
	public static int floor(double number) {
		return (int)Math.floor(number);
	}
	
	public static int ceil(double number) {
		return (int)Math.ceil(number);
	}
	
	public static double normalize(double value, double minValue, double maxValue) {
		return (value - minValue) / (maxValue - minValue);
	}

	public static double[] normalize(double[] values, double minValue, double maxValue) {
		double[] result = new double[values.length];
		for (int i = 0; i < values.length; i++) {
			result[i] = (values[i] - minValue) / (maxValue - minValue);
		}
		return result;
	}

	public static double denormalize(double normValue, double minValue, double maxValue) {
		return (normValue * (maxValue - minValue)) + minValue;
	}

	public static double[] denormalize(double[] normValues, double minValue, double maxValue) {
		double[] result = new double[normValues.length];
		for (int i = 0; i < normValues.length; i++) {
			result[i] = (normValues[i] * (maxValue - minValue)) + minValue;
		}
		return result;
	}
}
