### To import in Eclipse as Maven project
* Go to "File" -> "Import"
* In the "Import" dialog, expand the "Maven" folder and select "Existing Maven Projects"
* Click "Next"
* In the "Import Maven Projects" dialog, click "Browse" and locate your Maven project
* Click "Finish" to import the project

### To build in Eclipse as Maven project
* Right-click on the project and select "Run As" -> "Maven Build..."
* In the "Run Configurations" dialog, enter "clean package assembly:single" in the "Goals" field
* Click "Apply" and "Run" to run the Maven build with dependencies.

### Java Machine Learning library

A simple Machine Learning library for Java with the following objectives:
* easy-to-use
* easy-to-understand code
* support for best-known ML algorithms

Neural Networks Supported so far:
* Multi-Layer Perceptron (MLP)
* Recurrent Neural Network (RNN)
* Long-Short Term Memory (LSTM)

------------------------
### Useful links

[Activation functions](https://en.wikipedia.org/wiki/Activation_function)

##### Multi-Layer Perceptrons
* [How neural networks are trained](http://ml4a.github.io/ml4a/how_neural_networks_are_trained/)
* [A Derivation of Backpropagation in Matrix Form](https://sudeepraja.github.io/Neural/)
* [Backpropagation from the beginning](https://medium.com/@erikhallstrm/backpropagation-from-the-beginning-77356edf427d)
* [A Step by Step Backpropagation Example](https://mattmazur.com/2015/03/17/a-step-by-step-backpropagation-example/)

##### Recurrent Neural Networks
* [Recurrent Neural Network Gradients, and Lessons Learned Therein](http://willwolf.io/2016/10/18/recurrent-neural-network-gradients-and-lessons-learned-therein/)
* [Recurrent Neural Networks Tutorial](http://www.wildml.com/2015/09/recurrent-neural-networks-tutorial-part-1-introduction-to-rnns/)
* [Introduction to Recurrent Neural Networks](https://rubikscode.net/2018/03/12/introuduction-to-recurrent-neural-networks/)

##### Long-Short Term Memory Networks
* [Understanding LSTM Networks](http://colah.github.io/posts/2015-08-Understanding-LSTMs/)
* [Backpropogating an LSTM: A Numerical Example](https://medium.com/@aidangomez/let-s-do-this-f9b699de31d9)
* [Understanding LSTM and its diagrams](https://medium.com/mlreview/understanding-lstm-and-its-diagrams-37e2f46f1714)

------------------------

